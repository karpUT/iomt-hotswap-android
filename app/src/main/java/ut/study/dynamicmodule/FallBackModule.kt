package ut.study.dynamicmodule

class FallBackModule : IDynamicModule {
    override fun shouldNotifyDoctor(HR: Int): Boolean {
        return HR > 175
    }
}