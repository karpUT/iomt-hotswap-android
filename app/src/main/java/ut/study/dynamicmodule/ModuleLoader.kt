package ut.study.dynamicmodule

import android.util.Log
import dalvik.system.DexClassLoader
import java.io.File

class ModuleLoader(private val cacheDir: String) {

    fun load(dex: File?, cls: String = "ut.study.dynamicmodule.DynamicModule"): IDynamicModule {
        try {
            val classLoader = DexClassLoader(dex!!.absolutePath, cacheDir,
                    null, this.javaClass.classLoader)

            val moduleClass = classLoader.loadClass(cls)
            if (IDynamicModule::class.java.isAssignableFrom(moduleClass)) {
                return moduleClass.newInstance() as IDynamicModule
            }
        } catch (e: Exception) {
            Log.w("ModuleLoader", e.message)
            Log.d("ModuleLoader", "using FallBackModule")
        }
        return FallBackModule()
    }
}