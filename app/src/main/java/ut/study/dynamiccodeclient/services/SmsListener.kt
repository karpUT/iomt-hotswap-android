package ut.study.dynamiccodeclient.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony
import android.util.Log

// was used to record the timestamp when the notification sms was received

class SmsListener: BroadcastReceiver() {

    private lateinit var listener: Listener

    override fun onReceive(context: Context?, intent: Intent?) {

        var smsSender = ""
        var smsBody = ""

        for (smsMessage in Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
            smsSender = smsMessage.displayOriginatingAddress
            smsBody += smsMessage.messageBody
        }

        Log.d("SmsListener", "RECEIVER MESSAGE: $smsSender: $smsBody \n Received at: ${System.currentTimeMillis()}")
    }


    fun setListener(listener: Listener) {
        this.listener = listener
    }

    interface Listener {
        fun onTextReceived(text: String)
    }
}