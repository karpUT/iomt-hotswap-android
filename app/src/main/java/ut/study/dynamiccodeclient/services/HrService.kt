package ut.study.dynamiccodeclient.services


import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import polar.com.sdk.api.PolarBleApi
import polar.com.sdk.api.PolarBleApiCallback
import polar.com.sdk.api.PolarBleApiDefaultImpl
import polar.com.sdk.api.model.PolarDeviceInfo
import polar.com.sdk.api.model.PolarHrData
import ut.study.dynamiccodeclient.utils.HrHistory

class HrService : Service() {

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private val TAG = "HrService"
    private lateinit var api: PolarBleApi
    private var deviceID: String = ""
    private lateinit var hrHistory: HrHistory
    private var skipHr = false


    override fun onCreate() {

        val classContext = this
        super.onCreate()

        val localBroadcastManager = LocalBroadcastManager.getInstance(this)
        api = PolarBleApiDefaultImpl.defaultImplementation(this, PolarBleApi.FEATURE_HR)
        hrHistory = HrHistory(this)


        api.setApiCallback(object : PolarBleApiCallback {
            override fun blePowerStateChanged(b: Boolean) {
                Log.d(TAG, "BluetoothStateChanged $b")
            }

            override fun polarDeviceConnected(s: PolarDeviceInfo) {
                Log.d(TAG, "Device connected " + s.deviceId)
                Toast.makeText(classContext, "Connected",
                        Toast.LENGTH_SHORT).show()
            }

            override fun polarDeviceConnecting(polarDeviceInfo: PolarDeviceInfo) {
                Log.d(TAG, "Still connecting to ${polarDeviceInfo.deviceId}")
            }

            override fun polarDeviceDisconnected(s: PolarDeviceInfo) {
                Log.d(TAG, "Device disconnected $s")

            }

            override fun ecgFeatureReady(s: String) {
                Log.d(TAG, "ECG Feature ready $s")
            }

            override fun accelerometerFeatureReady(s: String) {
                Log.d(TAG, "ACC Feature ready $s")
            }

            override fun ppgFeatureReady(s: String) {
                Log.d(TAG, "PPG Feature ready $s")
            }

            override fun ppiFeatureReady(s: String) {
                Log.d(TAG, "PPI Feature ready $s")
            }

            override fun biozFeatureReady(s: String) {
                Log.d(TAG, "BIOZ Feature ready $s")
            }

            override fun hrFeatureReady(s: String) {
                Log.d(TAG, "HR Feature ready $s")
            }

            override fun fwInformationReceived(s: String, s1: String) {
                Log.d(TAG, "Firmware: " + s + " " + s1.trim { it <= ' ' })
            }

            override fun batteryLevelReceived(s: String, i: Int) {
                Log.d(TAG, "Battery level $s $i")
            }

            override fun polarFtpFeatureReady(s: String) {
                Log.d(TAG, "Polar FTP ready $s")
            }

            override fun hrNotificationReceived(s: String,
                                                polarHrData: PolarHrData) {
                if (!skipHr){
                    Log.d(TAG, "HR " + polarHrData.hr)
                    localBroadcastManager.sendBroadcast(Intent(HR_MESSAGE).putExtra("HR", polarHrData.hr))
                    hrHistory.addHr(polarHrData.hr)
                }
                skipHr = !skipHr
            }

        })
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        deviceID = intent!!.getStringExtra("id")
        hrHistory.userID = intent.getStringExtra("userId")
        api.connectToPolarDevice(deviceID)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        api.shutDown()
        Log.d(TAG, "Stopping HR service")
    }

    companion object {
        const val HR_MESSAGE = "hrMessage"
    }


}
