package ut.study.dynamiccodeclient.services

import android.content.*
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL


class ClientFirebaseMessagingService : FirebaseMessagingService() {

    val TAG = "firebaseService"
    var userCode = ""
    val baseUrl = "http://ec2-13-53-188-126.eu-north-1.compute.amazonaws.com/firebase"
    lateinit var name: String
    lateinit var localBroadcastManager: LocalBroadcastManager
    lateinit var requestQueue: RequestQueue

    override fun onCreate() {
        super.onCreate()
        localBroadcastManager = LocalBroadcastManager.getInstance(this)
        requestQueue = Volley.newRequestQueue(this)
        Log.d(TAG, "Firebase started")
    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        localBroadcastManager.sendBroadcast(Intent(FIREBASE_TOKEN_RECEIVED).putExtra("token", token!!))
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        super.onMessageReceived(message)
        Log.d(TAG, "From: " + message!!.from)
        Log.d(TAG, "File Url: " + message.data["url"]) // data.url
        val urlText = message.data["url"]

        if (urlText != null) {
            try {
                val parts = urlText.split("/")
                val path = applicationInfo.dataDir + "/files/"
                val pathAsPath = File(path)

                if (!pathAsPath.isDirectory) {
                    pathAsPath.mkdir()
                }

                val fileName = parts[parts.size - 1]
                val url = URL(urlText)
                val connection = url.openConnection()
                connection.connect()
                val input = BufferedInputStream(url.openStream(),
                        8192)
                val stamp = System.currentTimeMillis()
                val output = FileOutputStream(path + stamp + fileName)
                Log.d(TAG, "fileLoc: $path$stamp$fileName")


                val data = ByteArray(1024)
                var count = input.read(data)

                while (count != -1) {
                    output.write(data, 0, count)
                    count = input.read(data)
                }

                output.flush()
                output.close()
                input.close()
                Log.d(TAG, "finished downloading file: $fileName")
                localBroadcastManager.sendBroadcast(Intent(FIREBASE_FILE_RECEIVED).putExtra("file", "$stamp$fileName"))
            } catch (e: Exception) {
                Log.e(TAG, e.message)
            }
        }

    }

    companion object {
        const val FIREBASE_FILE_RECEIVED = "fileReceived"
        const val FIREBASE_TOKEN_RECEIVED = "tokenReceived"
    }

}