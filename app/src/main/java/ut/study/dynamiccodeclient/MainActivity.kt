package ut.study.dynamiccodeclient

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.text.SpannableStringBuilder
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import ut.study.dynamiccodeclient.services.ClientFirebaseMessagingService
import ut.study.dynamiccodeclient.services.HrService
import ut.study.dynamiccodeclient.services.SmsListener
import ut.study.dynamiccodeclient.utils.*
import java.net.URLEncoder

private const val baseUrl = "http://ec2-13-53-188-126.eu-north-1.compute.amazonaws.com/firebase"
private const val TAG = "mainActivity"

class MainActivity : AppCompatActivity() {

    private var activeDexFile = ""
    private var token = ""
    private var path = ""
    private var polarId = ""
    private var HR: Int = 0

    private lateinit var localBroadcastManager: LocalBroadcastManager
    private lateinit var polarIntent: Intent
    private lateinit var requestQueue: RequestQueue
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var hrNotificator: HrNotificator
    private lateinit var smsListener: SmsListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObjects()
        setIntitialFieldValues()
        askForPermissions()
        createBroadcastReceivers()
        createButtonListeners()
    }


    private fun initObjects() {
        setContentView(R.layout.activity_main)
        path = applicationInfo.dataDir + "/files/"
        localBroadcastManager = LocalBroadcastManager.getInstance(this)
        polarIntent = Intent(this, HrService::class.java)
        requestQueue = Volley.newRequestQueue(this)
        sharedPreferences = this.getPreferences(Context.MODE_PRIVATE)
        hrNotificator = HrNotificator(this)
    }


    private fun setIntitialFieldValues() {
        createDir(path)
        var fallback = getFirstDexFileFromDir(path)
        if (fallback == null) {
            fallback = ""
        }
        activeDexFile = sharedPreferences.getString("dexfile", fallback)!!
        firebaseUser.text = SpannableStringBuilder(sharedPreferences.getString("firebaseUser", "User ID")!!)
        polarId = sharedPreferences.getString("polarId", "")!!
        token = sharedPreferences.getString("token", "")!!
        hrNotificator.phone = sharedPreferences.getString("doctorPhone", "55669161")!!
    }


    private fun createButtonListeners() {
        startServices.setOnClickListener {
            startServices()
        }

        stopServices.setOnClickListener {
            stopService(polarIntent)
            Toast.makeText(this, "HR service stopped",
                    Toast.LENGTH_SHORT).show()
        }
    }

    private fun createBroadcastReceivers() {

        // new dex file pushed from server
        localBroadcastManager.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val fileName = intent.getStringExtra("file")
                if (fileExists(path, fileName)) {
                    activeDexFile = fileName
                    sharedPreferences.edit().putString("dexfile", activeDexFile).apply()
                }
                clearUnusedFiles(path, activeDexFile)
            }
        }, IntentFilter(ClientFirebaseMessagingService.FIREBASE_FILE_RECEIVED))


        // new token sent
        localBroadcastManager.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                token = intent!!.getStringExtra("token")
                sharedPreferences.edit().putString("token", token).apply()
            }
        }, IntentFilter(ClientFirebaseMessagingService.FIREBASE_TOKEN_RECEIVED))


        //new HR value sent by Polar
        localBroadcastManager.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                HR = intent!!.getIntExtra("HR", 0)
                analyzeHr(HR)
            }
        }, IntentFilter(HrService.HR_MESSAGE))
    }


    private fun startServices() {
        val userId = firebaseUser.text.toString()
        sendTokenToServer(userId)
        sharedPreferences.edit().putString("firebaseUser", userId).apply()
        hrNotificator.userID = userId

        val url = "$baseUrl/user?id=" + URLEncoder.encode(userId, "UTF-8")
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
                Response.Listener { response ->
                    if (response.has("PolarID")) {
                        polarId = response.getString("PolarID")
                        startBtService(polarId, userId)
                        sharedPreferences.edit().putString("polarId", polarId).apply()
                    }
                    if (response.has("doctorPhone")) {
                        hrNotificator.phone = response.getString("doctorPhone")
                        sharedPreferences.edit().putString("doctorPhone", hrNotificator.phone).apply()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(this, "Failed to retrieve user info", Toast.LENGTH_LONG).show()
                    Log.e(TAG, "/user error: $error")
                }
        )
        requestQueue.add(jsonObjectRequest)
    }

    private fun startBtService(deviceID: String, userID:String) {
        checkBT()
        Log.d(TAG, "Starting HR service with polar id: $deviceID")
        Toast.makeText(this, "Connecting to $deviceID", Toast.LENGTH_SHORT).show()
        polarIntent.putExtra("id", deviceID)
        polarIntent.putExtra("userId", userID)
        startService(polarIntent)

    }

    private fun checkBT() {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, 2)
        }
    }

    private fun askForPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            this.requestPermissions(arrayOf(
                    Manifest.permission.INTERNET,
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.RECEIVE_SMS
            ), 1)
        }
    }


    private fun analyzeHr(HR: Int) {
        hrText.text = "$HR"
        if (loadDexFile(path, activeDexFile).shouldNotifyDoctor(HR)) {
            Log.e(TAG, "Should notify doctor, HR: $HR")
            hrText.setTextColor(Color.parseColor("#BB0000"))
            hrNotificator.setCriticalStatus(true)
        } else {
            hrText.setTextColor(Color.parseColor("#00BB00"))
            hrNotificator.setCriticalStatus(false)
        }
    }

    private fun sendTokenToServer(userID: String){
        val body = JSONObject()
        body.put("id", userID);
        body.put("fields", JSONObject().put("token", token))

        Log.d(TAG, "token registration:$body")

        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, "$baseUrl/update", body,
                Response.Listener {
                    Log.d(TAG, "token registrated, value: $token")
                },
                Response.ErrorListener { error ->
                    Log.e(TAG, "token registration error: $error")
                }
        )
        requestQueue.add(jsonObjectRequest)
    }

}
