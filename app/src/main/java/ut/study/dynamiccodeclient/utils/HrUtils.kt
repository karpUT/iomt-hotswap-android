package ut.study.dynamiccodeclient.utils

import android.content.Context
import android.telephony.SmsManager
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

private const val TAG = "HrHistory"
private const val baseUrl = "http://ec2-13-53-188-126.eu-north-1.compute.amazonaws.com/firebase"

class HrHistory(context: Context) {

    private val requestQueue: RequestQueue = Volley.newRequestQueue(context)
    var hrHistory: MutableList<Int> = mutableListOf()
    var userID = ""


    fun addHr(hr: Int) {
        hrHistory.add(hr)
        if (hrHistory.size == 60) {
            sendHistoryDataToServer()
            hrHistory = mutableListOf()
        }
    }


    fun sendHistoryDataToServer() {
        val body = JSONObject()
        body.put("user", userID)
        body.put("heartRates", JSONArray(hrHistory))

        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, "$baseUrl/hrData", body,
                Response.Listener {
                    Log.d(TAG, "hrData sent to server, values: $body")
                },
                Response.ErrorListener { error ->
                    Log.e(TAG, "failed to send hrData to server: $error")
                }
        )
        requestQueue.add(jsonObjectRequest)
    }

}

class HrNotificator(context: Context) {

    private val requestQueue: RequestQueue = Volley.newRequestQueue(context)
    private val smsManger: SmsManager = SmsManager.getDefault()
    var isCritical = false
    var userID: String = ""
    var phone  = "+37255669161"

    fun setCriticalStatus(status: Boolean) {
        if (isCritical == !status) {
            notifyWebService(status)
            sendSmsNotification(status)
            isCritical = status
        }
    }

    private fun notifyWebService(isCritical: Boolean) {
        val body = JSONObject()
        body.put("user", userID)
        body.put("critical", if (isCritical) 1 else 0)

        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, "$baseUrl/notify", body,
                Response.Listener {
                    Log.d(TAG, "sent critical hr notification: $body")
                },
                Response.ErrorListener { error ->
                    Log.e(TAG, "failed to send critical hr notification: $error")
                }
        )
        requestQueue.add(jsonObjectRequest)
    }

    private fun sendSmsNotification(isCritical: Boolean){
        val message = "UserId: $userID status has changed to: ${if (isCritical) "Critical" else "Not Critical"} "
        smsManger.sendTextMessage(phone, null, message, null, null)
        Log.d("SmsListener", "SENT MESSAGE: $message ")
    }
}


