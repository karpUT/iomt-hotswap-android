package ut.study.dynamiccodeclient.utils

import ut.study.dynamicmodule.ModuleLoader
import java.io.File


/* cretates dir to specific path, returns false when dir already exists */
fun createDir(path: String): Boolean {
    val pathAsPath = File(path)
    if (!pathAsPath.exists() || !pathAsPath.isDirectory) {
        pathAsPath.mkdir()
        return true
    }
    return false
}


fun getFirstDexFileFromDir(path: String): String? {
    val files = File(path).listFiles()
    for (i in files.indices) {
        if (!files[i].name.endsWith(".dex")) {
            return files[i].name
        }
    }
    return null
}


fun clearUnusedFiles(path: String, activeDexFile: String) {
    val files = File(path).listFiles()
    for (i in files.indices) {
        if (!files[i].name.contains(activeDexFile)) {
            files[i].delete()
        }
    }
}

fun fileExists(path: String, filename: String): Boolean {
    val fileObject = File(path + filename)
    return (fileObject.exists() && fileObject.isFile)
}


fun getFile(path: String, filename: String): File? {
    return try {
        File(path + filename)
    } catch (e: Exception) {
        null
    }
}

fun loadDexFile(dir: String, filename: String) = ModuleLoader(dir).load(getFile(dir, filename))




