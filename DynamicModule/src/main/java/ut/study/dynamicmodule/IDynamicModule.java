package ut.study.dynamicmodule;

public interface IDynamicModule {
    Boolean shouldNotifyDoctor(int HR);
}
