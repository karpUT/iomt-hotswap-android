package ut.study.dynamicmodule;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DynamicModule implements IDynamicModule {

    @Override
    public Boolean shouldNotifyDoctor(int HR) {

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        boolean isDay = (hour >= 8 && hour < 20);

        if (isDay && HR > 120) return true;
        else if (!isDay && HR > 100) return true;
        else return HR < 30;
    }
}
