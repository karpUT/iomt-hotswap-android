### Android Application for Bachelor's thesis:
**Android-based Fog Computing Gateway with Over-The-Air Programming for personalized Health Monitoring**

by Kristo Karp

---

> **THE API server endpoint and Firebase keys will be revoked on June 20th, 2019 to avoid unknown parties from abusing them**

---

## Requirements

 - Android Studio + Android SDK with API version 23+
 - Polar H7 heart rate sensor
 - git (optional)

## Setup
To download the application then use:

    git clone https://karpUT@bitbucket.org/karpUT/iomt-hotswap-android.git
afterward import the created application into Android studio and build + run.

## Configuration

There is no configuration to set up the application, only thing to validate is that the server API URL that is used in the code is still valid.
API URL: `http://ec2-13-53-188-126.eu-north-1.compute.amazonaws.com/firebase`
this can also be changed to in case the server is started in your local machine.

---

> **In case of any issues occur the author can be contacted at Kristo.Karp[at]gmail.com**
